require 'rack-proxy'

class ProxyToGitlab < Rack::Proxy

  def perform_request(env)
    request = Rack::Request.new(env)

    if request.path =~ %r{^/gitlab}

      env["HTTP_HOST"] = "gitlab.com"
      env["SERVER_PORT"] = 80
      env['REQUEST_PATH'] = env['REQUEST_PATH'].gsub('/gitlab', '')
      env['PATH_INFO'] = env['PATH_INFO'].gsub('/gitlab', '')
      env['REQUEST_URI'] = env['REQUEST_URI'].gsub('/gitlab','')
      super(env)
    else
      @app.call(env)
    end
  end

end
