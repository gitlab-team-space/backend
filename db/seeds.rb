# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require_relative './seeds/helper'

puts 'Populating data...'

User.destroy_all
u = User.create!(email: 'admin@localhost.com', password: '12345678', password_confirmation: '12345678')
u.confirm

u = User.create!(email: 'leandronunes@gmail.com', password: '12345678', password_confirmation: '12345678', nickname: 'leandronunes')
u.confirm

repository = Repository.create!(name: 'Gitlab', endpoint: 'https://gitlab.com/api/v4')

u.repositories << repository
access = u.repository_accesses.first
access.private_token = 'fxrV4mvKshB5nPc7yBF2'
access.save!

Project.destroy_all
1.upto(20) do |n|
  Project.create(name: "Project #{n}", description: Faker::Lorem.sentence)
end


