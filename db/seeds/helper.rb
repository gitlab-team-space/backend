def create_user(params = {})
  name = params[:name] || Faker::Name.name

  User.create(email: Faker::Internet.email(name), password: '12345678', password_confirmation: '12345678', name: name)
end

def get_user(params = {})
  offset = rand(User.count)
  user = User.offset(offset).first
  user ||= create_user(params)
  user
end
