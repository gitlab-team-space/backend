class CreateRepositoryAccesses < ActiveRecord::Migration[5.0]
  def change
    create_table :repository_accesses do |t|
      t.string :private_token
      t.belongs_to :repository, index: true
      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
