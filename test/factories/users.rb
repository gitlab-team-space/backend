FactoryGirl.define do
  factory :user do
    name  Faker::Name.unique.first_name
    nickname { Faker::Internet.user_name(name) }
    email { Faker::Internet.email(name) }
    password '12345678'
    password_confirmation '12345678'
  end
end
