require 'test_helper'

class ProjectTest < ActiveSupport::TestCase

   test "gitlab_project_ids is an empty array by default" do
     project = Project.new
     assert_equal [], project.gitlab_project_ids
   end

   test "save gitlab_project_ids in database" do
     project = create(:project)
     project.gitlab_project_ids = [1,2,3]
     project.save
     project = Project.find(project.id)
     assert_equal [1,2,3], project.gitlab_project_ids
   end

end
