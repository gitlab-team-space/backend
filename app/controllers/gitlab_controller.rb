class GitlabController < ApplicationController

  before_action :authenticate_user!

  def index
    a = current_user.repository_accesses.first

    path = params.delete(:path)
    params.delete(:controller)
    params.delete(:action)

    redirect_to(a.repository.endpoint + '/' + path + '?' + params.map{|k,v| k + '=' + v}.join('&'))
  end

end
