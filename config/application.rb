require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
require "rails/test_unit/railtie"
require_relative "../lib/proxy_to_gitlab"
#require "proxy_to_gitlab"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GitlabTeamSpace
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true
#    config.middleware.use ProxyToGitlab, ssl_verify_none: true, streaming: false, read_timeout: 200
#    config.secret_key = 'd5d6219d8ef4d3e8e7e09ccf68ea75d9dfdc4ead59ce9811d9b54cbd38247cd18ee28fcce250bf1b51bf144b45901068e39bfe74f859fc7c049c44eae1220feg'

  end
end

#Rails.application.config.middleware.use ProxyToGitlab, streaming: false

