Rails.application.routes.draw do

  resources :repositories
  scope :api do
    resources :ranking do 
      collection do
        get :users
      end
    end

    resources :users
    resources :projects


    mount_devise_token_auth_for 'User', at: 'auth'

  end

  get 'gitlab/*path', to: 'gitlab#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
