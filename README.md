# README

[![build status](https://gitlab.com/gitlab-team-space/backend/badges/master/build.svg)](https://gitlab.com/gitlab-team-space/backend/commits/master)
[![coverage report](https://gitlab.com/gitlab-team-space/backend/badges/master/coverage.svg)](https://gitlab.com/gitlab-team-space/backend/commits/master)

* The project

API do frontend do projeto https://https://gitlab.com/gitlab-team-space/frontend

* Generate Diagrams

rake diagram:all

* Generate ERD diagram

bundle exec erd

* Gitlab Integrations

User gitlab integration with https://github.com/NARKOZ/gitlab
